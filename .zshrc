# ZSH configuration

OS=`uname`
FreeBSD="FreeBSD"
Linux="Linux"

ZSH=$HOME/.oh-my-zsh
ZSH_THEME="agnoster"
COMPLETION_WAITING_DOTS="true"
ENABLE_CORRECTION="true"
CMD_MAX_EXEC_TIME=5
plugins=(git npm)

## Enable prompt substitution
setopt PROMPT_SUBST

source $ZSH/oh-my-zsh.sh

precmd() { 
  print "" 
}

# ENV variables

DEFAULT_USER="glorieux"
DEFAULT_USERNAME='glorieux'
export EDITOR='vim'
export PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/X11/bin:/usr/local/bin:$PATH

## Golang environment variables

export GOROOT="/usr/local/go"
export GOPATH="/home/glorieux/dev/go"
export PATH="$PATH:$GOROOT/bin:$GOPATH/bin"

## Development environment variables
export PATH="$PATH:$HOME/dev/bin"

## UTF-8 environment variables
export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

# Aliases

## Directory Listing
if [[ $OS == LINUX ]]; then
  alias ls='ls --color=auto'
elif [[ $OS == FreeBSD ]]; then
  alias ls='ls -G'
fi
alias la='ls -lah'
alias ll='ls -lh'
alias lll=ll
alias l=ll

## Make aliases
if [[ $OS == LINUX ]]; then
  alias mk="make"
  alias mkcl="make clean"
  alias mkb="make build"
elif [[ $OS == FreeBSD ]]; then
  alias mk="gmake"
  alias mkcl="gmake clean"
  alias mkb="gmake build"
fi

## System Aliases
if [[ $OS == LINUX ]]; then
  alias sysupdate="sudo apt-get update"
  alias sysupgrade="sudo apt-get upgrade"
  alias sysclean="sudo apt-get autoclean"
  alias sysup="sysupdate && sysupgrade && sysclean"
  alias sysinstall="sudo apt-get install"
elif [[ $OS == FreeBSD ]]; then
  alias sysclean="sudo pkg autoremove && sudo pkg clean"
  alias sysaudit="sudo pkg audit -F"
  alias sysupdate="sudo pkg update && sudo portsnap fetch && sudo portsnap update && sudo freebsd-update fetch"
  alias sysupgrade="sudo pkg upgrade && sysclean && sysaudit && sudo freebsd-update install"
  alias sysinstall="sudo pkg install"
fi

## Docker Aliases
### Kills all running containers.
alias dockerkillall='sudo docker kill $(sudo docker ps -q)'

### Delete all stopped containers.
alias dockercleanc='printf "\n>>> Deleting stopped containers\n\n" && sudo docker rm $(sudo docker ps -a -q)'

### Delete all untagged images.
alias dockercleani='printf "\n>>> Deleting untagged images\n\n" && sudo docker rmi $(sudo docker images -q -f dangling=true)'

### Delete all stopped containers and untagged images.
alias dockerclean='dockercleanc || true && dockercleani'

## Launch Chrome without file access security
alias chrome_file_access=/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --allow-file-access-from-files

## File manipulation alias
alias lower_case_all="for i in * ;do; mv -f "$i" `echo "$i" | tr A-Z a-z`; done"
alias upper_case_all="for i in * ;do; mv -f "$i" `echo "$i" | tr a-z A-Z`; done"

## IP addresses
alias pubip="dig +s1hort myip.opendns.com @resolver1.opendns.com"
alias localip="sudo ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'"
alias ips="sudo ifconfig -a | grep -o 'inet6\? \(addr:\)\?\s\?\(\(\([0-9]\+\.\)\{3\}[0-9]\+\)\|[a-fA-F0-9:]\+\)' | awk '{ sub(/inet6? (addr:)? ?/, \"\"); print }'"

## View HTTP traffic
alias sniff="sudo ngrep -d 'wlp3s0' -t '^(GET|POST) ' 'tcp and port 80'"
alias httpdump="sudo tcpdump -i en1 -n -s 0 -w - | grep -a -o -E \"Host\: .*|GET \/.*\""

## HTTP requests
for method in GET HEAD POST PUT DELETE TRACE OPTIONS; do
	alias "$method"="lwp-request -m '$method'"
done

## Copy file interactive
alias cp='cp -i'

## Move file interactive
alias mv='mv -i'

## Remove file interactive
alias rm='rm -i'

## untar
alias untar='tar xvf'

# Local config
[[ -f ~/.zshrc.local ]] && source ~/.zshrc.local
