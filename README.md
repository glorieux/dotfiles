# DotFiles

Throughout our developer lives we will use many different machines and operating
systems. You can think of the dotfiles as the equivalent of an artisan toolbox. 

Here is mine.
Feel free to explore and take the bits and pieces you like.  
If you have any questions and/or comments please [create a new
issue](https://github.com/glorieux/dotfiles/issues/new).

## Prerequisites

On FreeBSD you'll need to install gnu make in order to install the dotfiles properly.

## Install

* Download or clone the repository

Running `make` or `gmake` on FreeBSD will do the following:

* Install the required software (git, vim, etc...)
* Symlink all the dotfiles to the HOME directory
* Run software plugin install procedures

It is safe to run `make` multiple times.

## Support

So far those dotfiles are meant to support Debian based operating systems as
well as macOS.

