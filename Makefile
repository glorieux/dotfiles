.PHONY: all install dev-setup

UNAME_S := $(shell sh -c 'uname -s 2>/dev/null || echo not')
UBUNTU_RELEASE := $(shell sh -c 'lsb_realease -c -s 2>/dev/null')  

all: link install dev-setup

link:
	@touch $(HOME)/.zshrc.local
	@for file in $(shell find $(CURDIR) -name ".*" -not -name ".gitignore" -not -name ".git" -not -name ".*.swp"); do \
		f=$$(basename $$file); \
		ln -sfn $$file $(HOME)/$$f; \
	done

install:

# Install packages
ifeq ($(UNAME_S),Linux)
	echo "deb http://debian.sur5r.net/i3/ $(UBUNTU_RELEASE) universe" | sudo tee -a /etc/apt/sources.list
	sudo apt-get update
	sudo apt-get --allow-unauthenticated install sur5r-keyring
	sudo apt-get update
	sudo apt-get install -y zsh i3 git vim wget curl ack htop feh ranger
endif
ifeq ($(UNAME_S),FreeBSD)
	sudo pkg upgrade
	sudo pkg install zsh git vim-lite wget curl htop ack
	sudo pkg autoremove
	sudo chsh -s /usr/local/bin/zsh $(USER)
ifeq ($(shell sh -c 'portmaster 2>/dev/null || echo not'),not)
	@echo "Portmaster is not installed"
	sudo make -C /usr/ports/ports-mgmt/portmaster install clean
endif
endif

# Install vim plugins
	mkdir -p ~/.vim/undodir
ifeq ($(wildcard ~/.vim/bundle/Vundle.vim),)
	git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
	vim +PluginInstall +GoInstallBinaries +qall
else
	vim +PluginUpdate +qall
endif

dev-setup:
	mkdir -p ~/dev/bin
